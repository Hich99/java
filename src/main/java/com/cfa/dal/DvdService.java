package com.cfa.dal;


import com.cfa.dto.Dvd;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class DvdService implements DvdInterface {

    EntityManagerFactory factory;
    EntityManager entityManager;


    public DvdService() {
        this.factory = Persistence.createEntityManagerFactory("DvdDB");
        this.entityManager = factory.createEntityManager();
    }

    @Override
    public List<Dvd> findAll() {
        List<Dvd> listing = new ArrayList<Dvd>();
        try {
            Query query = this.entityManager.createQuery("select d from Dvd d", Dvd.class);
            listing = query.getResultList();
        }catch (Exception e){

        }
        return listing;
    }

    @Override
    public Dvd findById(Integer id) {
        Dvd dvd = new Dvd();
        try {
            dvd = this.entityManager.find(Dvd.class, id);
        }catch (Exception e){
            e.getMessage();
        }
        return dvd;
    }

    @Override
    public Dvd updateDvd(Integer id, Dvd dvd) {
        try{
            Dvd oldDvd = this.entityManager.find(Dvd.class, id);
            this.entityManager.getTransaction().begin(); // revien a l'etat anterieur si ya un pb avec entier dans un string etc dans la bdd
            // modifi la table a partir du moment ou l'ensble de la tables est valider
            oldDvd.setTitre(dvd.getTitre());
            oldDvd.setGenre(dvd.getGenre());
            oldDvd.setRate(dvd.getRate());
            oldDvd.setDuree(dvd.getDuree());

            this.entityManager.getTransaction().commit();
            this.closed();

            return oldDvd;
        }catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    private void closed() {
        this.entityManager.close();
        this.factory.close();
    }

    @Override
    public void addDvd(Dvd dvd) {
        try {
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(dvd);
            this.entityManager.getTransaction().commit();
            this.closed();
        }catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public void delDvd(Integer id) {
        try {
            Dvd dvd = this.entityManager.find(Dvd.class, id);
            this.entityManager.getTransaction().begin();
            this.entityManager.remove(dvd);
            this.entityManager.getTransaction().commit();
            this.closed();
        }catch (Exception e){
            e.getMessage();
        }
    }
}
