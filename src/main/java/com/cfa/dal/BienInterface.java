package com.cfa.dal;

import com.cfa.dto.Bien;

import java.util.List;

public interface BienInterface {
    List<Bien> findAll();
    Bien findById(Integer id);
    Bien updateDvd(Integer id, Bien bien);
    void addBien(Bien dvd);
    void delBien(Integer id);

}
