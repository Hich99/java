package com.cfa.controler;

import com.cfa.dal.DvdService;
import com.cfa.dto.Dvd;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "DvdServlet",urlPatterns = {"/dvd"})
public class DvdServlet extends HttpServlet {

    //Method get et post
    private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        DvdService dvds = new DvdService();
        if (req.getParameter("init") != null){
            String test = req.getParameter("init");
            switch (test){
                case "new":
                    Dvd ndvd = new Dvd(req.getParameter("titre"),req.getParameter("duree"),req.getParameter("genre"),Integer.parseInt(req.getParameter("rate") ));
                    dvds.addDvd(ndvd);
                    break;

                case "modif" :
                    Dvd mDvd = dvds.findById(Integer.parseInt(req.getParameter("id")));
                    mDvd.setTitre(req.getParameter("titre"));
                    mDvd.setDuree(req.getParameter("duree"));
                    mDvd.setGenre(req.getParameter("genre"));
                    mDvd.setRate(Integer.parseInt(req.getParameter("rate")));
                    dvds.updateDvd(Integer.parseInt(req.getParameter("id")),mDvd);
                    break;
                case "supr":
                    dvds.delDvd(Integer.parseInt(req.getParameter("id")));
                    break;

            }
            req.setAttribute("init",null);

        }

        req.setAttribute("listDvd", this.getAllDvd());
        RequestDispatcher rdisp = req.getRequestDispatcher("listDvd.jsp");
        rdisp.forward(req,resp);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    //methode pour retourner tout les dvd
    private List<Dvd> getAllDvd(){
        DvdService dvds = new DvdService();
        List<Dvd> theList = new ArrayList<Dvd>();
        theList = dvds.findAll();

        System.out.println("the list" + theList);
        return theList;
    }

}
