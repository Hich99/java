package com.cfa.controler;

import com.cfa.dal.DvdService;
import com.cfa.dal.BienService;
import com.cfa.dto.Dvd;
import com.cfa.dto.Bien;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ModifServlet",urlPatterns = {"/modif"})
public class ModifServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DvdService dvds = new DvdService();
        BienService bienService = new BienService();

        Dvd dvd = dvds.findById(Integer.parseInt(req.getParameter("id")));
        Bien bien = bienService.findById(Integer.parseInt(req.getParameter("id")));


        req.setAttribute("dvd",dvd);
        req.setAttribute("bien", bien);
        req.setAttribute("isUpdate","ok");
        RequestDispatcher rdisp = req.getRequestDispatcher("ajoutForm.jsp");
        rdisp.forward(req,resp);
    }
}
