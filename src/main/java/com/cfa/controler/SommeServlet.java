package com.cfa.controler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SommeServlet", urlPatterns = {"/somme"})
public class SommeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
        HttpSession session = req.getSession();
        if (session.getAttribute("connected") != null && session.getAttribute("connected") == "true"){
            System.out.println("on est bien connecter");
        }else {
            RequestDispatcher rdisp = req.getRequestDispatcher("hello");
            rdisp.forward(req,resp);
            System.out.println("on est pas co");
        }

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String err = "Bon"; // 3cas bon vide char


        String num1 = req.getParameter("num1");
        String num2 = req.getParameter("num2");


        StringBuilder res = new StringBuilder();
        res.append("<html><body>");


        if (num1.isEmpty() || num2.isEmpty()){
            err = "Vide";
        }

        if (err == "Bon" && (num1.matches("\\d")) && (num2.matches("\\d")) ){
            err = "Char";

        }

        if (err== "char"){

        }
        switch (err){
            case "Bon":
                int somme = Integer.parseInt(num1) + Integer.parseInt(num2);
                res.append("La somme de num1:  (" + num1 + ") ");
                res.append("et de num2:  (" + num2 + ") <br/>");
                res.append("Est égale à: "+ somme);
                System.out.println(req.getMethod());
                RequestDispatcher rdisp = null;

                if (req.getMethod().equalsIgnoreCase("GET")){
                    rdisp =req.getRequestDispatcher("index.jsp");
                    req.setAttribute("resultat", "le resultat est :" + somme);
                }else {//cas post
                    rdisp = req.getRequestDispatcher("saisie.jsp");
                    req.setAttribute("somme", somme);
                }
                rdisp.forward(req,resp);

                break;
            case "Vide":
                res.append("Un des champs est vide");
                res.append("<a href='saisie.jsp'> Retour </a>");
                break;
            case "Char":
                res.append("Un des contient des lettres");
                res.append("<a href='saisie.jsp'> Retour </a> <br />");

                res.append("num1 string:  " + num1.toString() + "<br/>");
                res.append("num1 lentgh:  " + num1.length());

                break;
        }

        res.append("</body></html>");

        out.println(res.toString());


    }
}
