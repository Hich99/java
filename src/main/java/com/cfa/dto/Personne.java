package com.cfa.dto;

import javax.persistence.*;


public class Personne {
    private Integer id;
    private String nom;
    private String prenom;
    private String email;
    private String mdp;


    public Personne(){

    }



    /**
     *
     * @param nom
     * @param prenom
     * @param email
     */
    public Personne(String nom, String prenom, String email,String mdp) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.mdp = mdp;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }




    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                '}';
    }


    /**
     *
     * @return nom + prenom
     */
    public String getFullName(){
     return this.nom + " " + this.prenom;
    }
}
