package com.cfa.dto;


import javax.persistence.*;

@Entity
@Table(name = "dvd")
public class Dvd {

    private Integer id;
    private String titre;
    private String duree;
    private String genre;
    private Integer rate;

    public Dvd() {
    }

    public Dvd(String titre, String duree, String genre, Integer rate) {
        this.titre = titre;
        this.duree = duree;
        this.genre = genre;
        this.rate = rate;
    }

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Column(name = "duree")
    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    @Column(name = "genre")
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Column(name = "rate")
    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Dvd{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", duree='" + duree + '\'' +
                ", genre='" + genre + '\'' +
                ", rate=" + rate +
                '}';
    }
}
