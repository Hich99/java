<!doctype html>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="CSS/main.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>

<form class="form w-50 bg-dark m-auto p-5" action="login" method="post">
    <div class="form-group">
        <input class="form-control" type="email" name="email"  placeholder="Email" />
    </div>
    <div class="form-group">
        <input class="form-control" type="password" name="mdp"  placeholder="Mot de passe"/>
    </div>
    <div class="form-group">
        <input class=" btn btn-default btn-primary" type="submit" name="submit" value="Conection"/>
        <a href="index.jsp" class=" btn btn-default btn-warning">
            Anuler
        </a>
    </div>
    <c:if test="${errMsg != null}">
        <p class="alert alert-danger"> ${errMsg}</p>
    </c:if>
</form>
</body>
</html>