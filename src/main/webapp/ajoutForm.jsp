<!doctype html>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>
    <jsp:include page="link.jsp" />
</head>
<body>
    <div class="container mt-5">
        <form action="dvd" method="post">
            <c:choose>
                <c:when test="${isUpdate}">
                    <input type="hidden" name="init" value="modif">
                    <input type="hidden" name="id" value="${dvd.id}">
                </c:when>
                <c:otherwise>
                    <input type="hidden" name="init" value="new">
                </c:otherwise>
            </c:choose>
            <input type="hidden" name="init" value="new">

            <div class="group-form">
                <label> Titre </label>
                <input class="form-control" type="text" name="titre" value="${dvd.titre}"/>
            </div>

            <div class="group-form">
                <label>Duree</label>
                <input class="form-control" type="text" name="duree" value="${dvd.duree}"/>
            </div>

            <div class="group-form">
                <label>Genre</label>
                <input class="form-control" type="text" name="genre" value="${dvd.genre}" />
            </div>

            <div class="group-form">
                <label>Rate</label>
                <input class="form-control" type="number" min="0" max="10" name="rate" value="${dvd.rate}" />
            </div>

            <div class="group-form">
                <input type="submit" value="enregistrer" class="btn btn-default btn-info"/>
            </div>

        </form>

    </div>
</body>
</html>