<!doctype html>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des DVD</title>
    <jsp:include page="link.jsp" />
</head>
<body>

    <div class="container mt-5">
        <div class="jumbotron">
            <a class="float-right" href="index.jsp">Home</a>
            <h2>Liste des DVD</h2>
        </div>
        <div>
            <a href="ajoutForm.jsp" class="btn btn-default btn-success float-right">Create dvd</a>
        </div>


        <table class="table table-stripe mt-5">
        <thead>
            <tr>
                <td>Id</td>
                <td>Titre</td>
                <td>Duree</td>
                <td>Genre</td>
                <td>Rate</td>
                <td>Action</td>
            </tr>
        </thead>

        <tbody>
            <c:forEach items="${listDvd}" var="dvd">
                <tr>
                    <td>${dvd.id}</td>
                    <td>${dvd.titre}</td>
                    <td>${dvd.duree}</td>
                    <td>${dvd.genre}</td>
                    <td>${dvd.rate}</td>
                    <td class="d-flex flex-row justify-content-around gap">
                        <form action="modif" method="post">
                            <input type="hidden" name="init" value="modif">
                            <input type="hidden" name="id" value="${dvd.id}">
                            <input type="submit" value="up" class="btn btn-default btn-warning ">
                        </form>
                        <form action="dvd" method="post">
                            <input type="hidden" name="init" value="supr">
                            <input type="hidden" name="id" value="${dvd.id}">
                            <input type="submit" value="del" class="btn btn-default btn-danger ">
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    </div>

</body>
</html>